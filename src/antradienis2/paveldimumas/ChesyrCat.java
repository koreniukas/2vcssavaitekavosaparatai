package antradienis2.paveldimumas;

/**
 * Created by rkore on 10/10/2017.
 */
public class ChesyrCat extends Cat{
    final boolean SMILES;

    public ChesyrCat(String name, String color) {
        super(name, color);
        this.SMILES=true;
    }
}
