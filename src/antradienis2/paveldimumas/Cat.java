package antradienis2.paveldimumas;

/**
 * Created by rkore on 10/10/2017.
 */
public class Cat extends Animal{
    private String color;
    final boolean HAS_TAIL= true;

    public Cat(String name, String color) {
        super(name);
        this.color = color;
    }

    public void sayMew (){
        System.out.println("miau miau miau");
    }

    @Override
    public void move() {
        System.out.println("Katukas greitai bega");
    }
}
