package antradienis2.paveldimumas;

/**
 * Created by rkore on 10/10/2017.
 */
public class Cow extends Animal{

    private final int RAGAI = 2;
    private boolean givesMilk;

    public Cow(String name, boolean givesMilk) {
        super(name);
        this.givesMilk = givesMilk;
    }

    @Override
    public void move() {
        System.out.println("Lengvai is leto siubuodama");
    }


    @Override
    public void eat(int maistas) {
        System.out.println("Eda zole ir atrajoja");
        maistas--;
        maistas++;
        maistas--;
    }
}
