
package pirmadienis2.coffemachinesimple;

/**
 * Created by rkore on 10/9/2017.
 */
public class CoffeAparatus {
    private final String name;
    private final int sugarCapacity;
    private final int coffeCapacity;
    private final double waterCapacity;
    private int sugarLeft;
    private int coffeLeft;
    private double waterLeft;
    private int usageCounter;
    private final int MAX_USED = 100;

    public CoffeAparatus(String name, int sugarCapacity, int coffeCapacity, int waterCapacity) {
        this.name = name;
        this.sugarCapacity = sugarCapacity;
        this.coffeCapacity = coffeCapacity;
        this.waterCapacity = waterCapacity;
        this.sugarLeft = 0;
        this.coffeLeft = 0;
        this.waterLeft = 0;
        this.usageCounter = 0;
    }

    public String getName() {
        return name;
    }

    public int getSugarCapacity() {
        return sugarCapacity;
    }

    public int getCoffeCapacity() {
        return coffeCapacity;
    }

    public double getWaterCapacity() {
        return waterCapacity;
    }

    public int getSugarLeft() {
        return sugarLeft;
    }

    public int getCoffeLeft() {
        return coffeLeft;
    }

    public double getWaterLeft() {
        return waterLeft;
    }

    public int getUsageCounter() {
        return usageCounter;
    }

    public int getMAX_USED() {
        return MAX_USED;
    }

    public boolean fillWater (double water){
        if (water <= (this.waterCapacity - this.waterLeft)){
            this.waterLeft += water;
            System.out.println("Aparatas papildytas vandeniu " + water + " l");
            return true;
        }else{
            System.out.println("Sumazinkyte norimo ipilti vandens kieki, aparato " + getName() + "talpa  yra tik" +
                    getWaterCapacity());
            return false;
        }
    }

    public boolean fillSugar(int sugar){
        if (sugar <= (this.sugarCapacity- this.sugarLeft)){
            this.sugarLeft += sugar;
            System.out.println("Aparatas papildytas cukrumi " + sugar + " g");
            return true;
        }else{
            System.out.println("Sumazinkyte norimo ipilti cukraus kieki, aparato " + getName() + " talpa  yra tik " +
                    getSugarCapacity() + "g");
            return false;
        }
    }

    public boolean fillCoffe (int coffe){
        if (coffe <= (this.coffeCapacity - this.coffeLeft)){
            this.coffeLeft += coffe;
            System.out.println("Aparatas papildytas kava " + coffe + " g");
            return true;
        }else{
            System.out.println("Sumazinkyte norimo ipilti kavos kieki, aparato " + getName() + " talpa  yra tik " +
                    getCoffeCapacity() + " g ");
            return false;
        }
    }

    public void makeCoffe (String coffeName){

        if(isReady()) {
            switch (coffeName) {
                case "Ekspresso":
                    this.coffeLeft -= 4;
                    this.waterLeft -= 0.1;
                    this.usageCounter++;
                    System.out.println("Ekspresso paruosta");
                    break;
                case "Juoda Amerikonska":
                    this.coffeLeft -= 4;
                    this.waterLeft -= 0.2;
                    this.sugarLeft -= 3;
                    this.usageCounter++;
                    System.out.println("Juoda Amerikietiska paruosta");
                    break;
                case "Vanduo su cukrum":
                    this.coffeLeft -= 0;
                    this.waterLeft -= 0.2;
                    this.sugarLeft -= 1;
                    this.usageCounter++;
                    System.out.println("Vanduo su cukrumi paruostas");
                    break;
                default:
                    System.out.println("Padarysiu gal as tau tiesiog kavos");
                    this.coffeLeft -= 2;
                    this.waterLeft -= 0.2;
                    this.sugarLeft -= 1;
            }
        }else {

            if (usageCounter == MAX_USED) {

                System.out.println("Valom aparata");
            }
            if (this.sugarLeft <= 4) {
                System.out.println("Cukraus truksta, reikalinga papildyti");

            }
            if (this.waterLeft <= 0.1) {
                System.out.println("Vandens nebera papildyk");

            }
            if (this.coffeLeft <= 4) {
                System.out.println("Papildom kavutes, aparatas tuscias");

            }
        }

    }

    public void clearAparatus () {
        System.out.println("Aparatas isvalytas");
        this.usageCounter = 0;
    }

    public boolean isReady () {
        if      ((this.coffeLeft > 0) && (this.coffeLeft <= this.coffeCapacity)&&
                (this.waterLeft > 0) && (this.waterLeft <=this.waterCapacity)&&
                (this.sugarLeft > 0) && (this.sugarLeft <= this.sugarCapacity)&&
                (this.usageCounter != this.MAX_USED)){
            System.out.println("Aparatas darbui pasiruoses");
            return true;
        }
        return false;

    }

    public void querryProd (){
        System.out.println("Kavos yra:"  +this.coffeLeft);
        System.out.println("Vandens yra: " + this.waterLeft);
        System.out.println("Cukraus yra: " + this.sugarLeft);
        System.out.println("Naudota : " + this.usageCounter);
    }


}




