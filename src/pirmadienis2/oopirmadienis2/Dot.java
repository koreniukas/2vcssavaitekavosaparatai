package pirmadienis2.oopirmadienis2;

/**
 * Created by rkore on 10/9/2017.
 */
public class Dot {
    private int x;
    private int y;
    private String color;

    public Dot(int x, int y, String color) {
        this.x = x;
        this.y = y;
        this.color = color;
    }
    public Dot(int x, int y) {
        this.x = x;
        this.y = y;
        this.color = "green";
    }
    public Dot(String color) {
        this.x = 1;
        this.y = 1;
        this.color = color;
    }


    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public String getColor() {
        return color;
    }

    public double returnDistanceToOtherDot (int x, int y){
       int deltax = Math.abs(this.x-x);
       int deltay = Math.abs(this.y-y);
       return (Math.sqrt(Math.pow(deltax,2)+Math.pow(deltay,2)));
    }

    public int returnCartesianQuarter (){
        int quarter = 0;
        if( this.x >0 && this.y >0) {quarter = 1;}
        if( this.x <0 && this.y >0) {quarter = 2;}
        if( this.x >0 && this.y <0) {quarter = 4;}
        if( this.x <0 && this.y <0) {quarter = 3;}
        return quarter;
    }

    public boolean sameQuarter (Dot dot){
        return this.returnCartesianQuarter()
                == dot.returnCartesianQuarter()?
                true:false;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Dot dot = (Dot) o;

        if (getX() != dot.getX()) return false;
        if (getY() != dot.getY()) return false;
        return getColor() != null ? getColor().equals(dot.getColor()) : dot.getColor() == null;
    }

    @Override
    public int hashCode() {
        int result = getX();
        result = 31 * result + getY();
        result = 31 * result + (getColor() != null ? getColor().hashCode() : 0);
        return result;
    }
}
