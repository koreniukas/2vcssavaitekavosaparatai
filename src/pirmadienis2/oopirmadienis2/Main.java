package pirmadienis2.oopirmadienis2;

/**
 * Created by rkore on 10/9/2017.
 */
public class Main {

    public static void main(String[] args) {
        Dot dot1 = new Dot(7,-2,"purple");
        Dot dot2 = new Dot (-11,-5,"green");

        System.out.println(dot2.returnCartesianQuarter());
        System.out.println(dot2.returnDistanceToOtherDot(dot1.getX(),dot1.getY()));

        System.out.println(dot1.returnCartesianQuarter());
        System.out.println(dot1.returnDistanceToOtherDot(dot2.getX(),dot2.getY()));
        System.out.println(dot1.sameQuarter(dot2));


    }

}
