package pirmadienis2.coffemachineextended;

/**
 * Created by rkore on 10/9/2017.
 */
public class ProductCapacity {
    private final int sugarCapacity;
    private final int coffeCapacity;
    private  final double waterCapacity;

    public ProductCapacity() {
        this(200,200,2);
    }

    public ProductCapacity(int sugarCapacity, int coffeCapacity, double waterCapacity) {
        this.sugarCapacity = sugarCapacity;
        this.coffeCapacity = coffeCapacity;
        this.waterCapacity = waterCapacity;
    }



    public int getSugarCapacity() {
        return sugarCapacity;
    }

    public int getCoffeCapacity() {
        return coffeCapacity;
    }

    public double getWaterCapacity() {
        return waterCapacity;
    }








}
