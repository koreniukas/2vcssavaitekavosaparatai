package pirmadienis2.coffemachineextended;

/**
 * Created by rkore on 10/10/2017.
 */
public class CoffeCup  extends Cup{
    private String name;
    private Consumables coffeIngredients;


    public CoffeCup(String name, int coffe, int sugar, double water) {
        this.name = name;
        this.coffeIngredients = new Consumables(sugar,coffe,water);
    }

    @Override
    public Consumables getCoffeIngredients() {
        return coffeIngredients;
    }

    @Override
    public String toString() {
        return "CoffeCup: " +
                "name:  " + name + '\'' +
                ", coffeIngredients : " + coffeIngredients ;
    }
}
