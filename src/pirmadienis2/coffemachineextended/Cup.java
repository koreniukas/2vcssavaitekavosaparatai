package pirmadienis2.coffemachineextended;

/**
 * Created by rkore on 10/10/2017.
 */
public class Cup {
    final int MAX_CAPACITY = 200;
    private Consumables coffeIngredients;
    private boolean prepared = false;
    public void printCapacity (){
        System.out.println(MAX_CAPACITY);
    }

    public Consumables getCoffeIngredients() {
        return coffeIngredients;
    }

    public void prepare (){
        this.prepared = true;
        System.out.println("Paruosta pagardinus polimorfizmo atveju");
    }

    @Override
    public String toString() {
        return "Cup{" +
                "prepared=" + prepared +
                '}';
    }
}
