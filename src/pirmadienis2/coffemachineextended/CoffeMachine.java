
package pirmadienis2.coffemachineextended;

/**
 * Created by rkore on 10/9/2017.
 */
public class CoffeMachine {
    private final String name;
    private ProductCapacity productCapacity;
    private Consumables consumables;

    private int usageCounter;
    private final int MAX_USED = 100;

    public void setProductCapacity(ProductCapacity productCapacity) {
        this.productCapacity = productCapacity;
    }

    public CoffeMachine() {
        this("Defoltinis", 200, 200, 4);
    }

    public CoffeMachine(String name, int sugarCapacity, int coffeCapacity, double waterCapacity) {
        this.productCapacity = new ProductCapacity(sugarCapacity, coffeCapacity, waterCapacity);
        this.name = name;
        this.consumables = new Consumables(0, 0, 0);
        this.usageCounter = 0;
    }

    public String getName() {
        return name;
    }

    public int getSugarCapacity() {
        return this.productCapacity.getSugarCapacity();
    }

    public int getCoffeCapacity() {
        return this.productCapacity.getCoffeCapacity();
    }

    public double getWaterCapacity() {
        return this.productCapacity.getWaterCapacity();
    }

    public int getSugarLeft() {
        return this.consumables.getSugarLeft();
    }

    public int getCoffeLeft() {
        return this.consumables.getCoffeLeft();
    }

    public double getWaterLeft() {
        return this.consumables.getWaterLeft();
    }

    public int getUsageCounter() {
        return usageCounter;
    }

    public int getMAX_USED() {
        return MAX_USED;
    }

    public boolean fillWater(double water) {
        if (water <= (this.productCapacity.getWaterCapacity() - this.consumables.getWaterLeft())) {
            this.consumables.setWaterLeft(this.consumables.getWaterLeft() + water);
            System.out.println("Aparatas papildytas vandeniu " + water + " l");
            return true;
        } else {
            System.out.println("Sumazinkyte norimo ipilti vandens kieki, aparato " + getName() + "talpa  yra tik" +
                    this.productCapacity.getWaterCapacity());
            return false;
        }
    }

    public boolean fillSugar(int sugar) {
        if (sugar <= (this.productCapacity.getSugarCapacity() - this.consumables.getSugarLeft())) {
            this.consumables.setSugarLeft(this.consumables.getSugarLeft() + sugar);
            System.out.println("Aparatas papildytas cukrumi " + sugar + " g");
            return true;
        } else {
            System.out.println("Sumazinkyte norimo ipilti cukraus kieki, aparato " + getName() + " talpa  yra tik " +
                    this.productCapacity.getSugarCapacity() + "g");
            return false;
        }
    }

    public boolean fillCoffe(int coffe) {
        if (coffe <= (this.productCapacity.getCoffeCapacity() - this.consumables.getCoffeLeft())) {
            this.consumables.setCoffeLeft(this.consumables.getCoffeLeft() + coffe);
            System.out.println("Aparatas papildytas kava " + coffe + " g");
            return true;
        } else {
            System.out.println("Sumazinkyte norimo ipilti kavos kieki, aparato " + getName() + " talpa  yra tik " +
                    getCoffeCapacity() + " g ");
            return false;
        }
    }

//    public boolean fillAlltoMax(){
//        this.consumables.s
//    }

    public Cup Consume(Cup cup) {
        this.consumables.setCoffeLeft(this.consumables.getCoffeLeft() - cup.getCoffeIngredients().getCoffeLeft());
        this.consumables.setWaterLeft(this.consumables.getWaterLeft() - cup.getCoffeIngredients().getWaterLeft());
        this.consumables.setSugarLeft(this.consumables.getSugarLeft() - cup.getCoffeIngredients().getSugarLeft());
        cup.prepare();
        this.usageCounter++;
        return cup;


    }

    public void setConsumables(Consumables consumables) {
        this.consumables = consumables;
    }

    public void makeCoffe(String coffeName) {

        if (isReady() && (this.usageCounter < MAX_USED)) {
            switch (coffeName) {
                case "Ekspresso":
                    Consume(new EspressoCup("Espresso", 2, 0, 0.1));
                    System.out.println("Ekspresso paruosta");
                    break;
                case "Juoda Amerikonska":
                    Consume(new AmeriicanCoffeCup("Juoda Amerikietiska", 2, 2, 0.2));
                    System.out.println("Juoda Amerikietiska paruosta");
                    break;
                case "Cappucino":

                    Consume(new CapuccinoCup("Capucino", 2, 3, 0.2));
                    System.out.println("Vanduo su cukrumi paruostas");
                    break;
                default:
                    System.out.println("Padarysiu gal as tau tiesiog kavos");
                    Consume(new CoffeCup("Coffe", 2, 1, 0.2));
            }
        } else {

            if (usageCounter == MAX_USED) {

                System.out.println("Valom aparata");
            }
            if (this.consumables.getSugarLeft() <= 4) {
                System.out.println("Cukraus truksta, reikalinga papildyti");

            }
            if (this.consumables.getWaterLeft() <= 0.1) {
                System.out.println("Vandens nebera papildyk");

            }
            if (this.consumables.getCoffeLeft() <= 4) {
                System.out.println("Papildom kavutes, aparatas tuscias");

            }
        }

    }

    public void clearAparatus() {
        System.out.println("Aparatas isvalytas");
        this.usageCounter = 0;
    }

    public void querryProd() {
        System.out.println("Kavos yra:" + this.consumables.getCoffeLeft());
        System.out.println("Vandens yra: " + this.consumables.getWaterLeft());
        System.out.println("Cukraus yra: " + this.consumables.getSugarLeft());
        System.out.println("Naudota : " + this.usageCounter);
    }

    public ProductCapacity getProductCapacity() {
        return productCapacity;
    }

    public boolean isReady() {
        if ((this.consumables.getCoffeLeft() > 0) &&
                (this.consumables.getWaterLeft() > 0) &&
                (this.consumables.getSugarLeft() > 0)) {
            System.out.println("Aparatas darbui pasiruoses");
            return true;
        }
        return false;

    }

}




