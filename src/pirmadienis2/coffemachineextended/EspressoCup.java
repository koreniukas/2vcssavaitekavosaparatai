package pirmadienis2.coffemachineextended;

/**
 * Created by rkore on 10/10/2017.
 */
public class EspressoCup extends Cup {
    private String name;
    private Consumables coffeIngredients;


    public EspressoCup(String name, int coffe, int sugar, double water) {
        this.name = name;
        this.coffeIngredients = new Consumables(coffe, sugar, water);
    }

    @Override
    public Consumables getCoffeIngredients() {
        return coffeIngredients;
    }


}
