package pirmadienis2.coffemachineextended;

import java.util.Scanner;

/**
 * Created by rkore on 10/9/2017.
 */


public class Main {

    private static Scanner scanner = new Scanner(System.in);

    public static void printMenu(){
        System.out.println("Sveiki atvyke prie kavos aparato, pasirinkite funkcija:" +
                "\n 0 - meniu" +
                "\n 1 - padaryti kavos" +
                "\n 2 - kiek ko yra?" +
                "\n 3 - papildyt vandens" +
                "\n 4 - paplidyt kavos" +
                "\n 5 - papildyt cukraus" +
                "\n 6 - isvalyti" +
                "\n 7 - iseiti");
    }

    public static void main(String[] args) {
        CoffeMachine manoKavosAparatas = new CoffeMachine("Kosminis",100,200,3);

        int choise = 0;
        boolean quit = false;

        printMenu();

        while (!quit){
            choise = scanner.nextInt();
            scanner.nextLine();
            switch (choise){
                case 0:
                    printMenu();
                    break;
                case 1:
                    System.out.println("Pasirinkite: Ekspresso, Juoda Amerikonska ar Vanduo su cukrum?");
                    String cof = scanner.nextLine();
                    manoKavosAparatas.makeCoffe(cof);
                    break;
                case 2:
                    manoKavosAparatas.querryProd();
                    break;
                case 3:
                    System.out.println("Kiek vandens pildysite?");
                    double amount = scanner.nextDouble();
                    manoKavosAparatas.fillWater(amount);
                    break;
                case 4:
                    System.out.println("Kiek kavos pildysite?");
                    int amount2 = scanner.nextInt();
                    manoKavosAparatas.fillCoffe(amount2);
                    break;
                case 5:
                    System.out.println("Kiek cukraus pildysite?");
                    int amount3 = scanner.nextInt();
                    manoKavosAparatas.fillSugar(amount3);
                    break;
                case 6:
                    manoKavosAparatas.clearAparatus();
                    break;
                case 7:
                    quit = true;
                    break;

            }

        }


    }
}
