package pirmadienis2.coffemachineextended;

/**
 * Created by rkore on 10/10/2017.
 */
public class Consumables {

    private int sugarLeft;
    private int coffeLeft;
    private double waterLeft;



    public Consumables(int sugarLeft, int coffeLeft, double waterLeft) {
        this.sugarLeft = sugarLeft;
        this.coffeLeft = coffeLeft;
        this.waterLeft = waterLeft;

    }

    public int getSugarLeft() {
        return sugarLeft;
    }

    public int getCoffeLeft() {
        return coffeLeft;
    }

    public double getWaterLeft() {
        return waterLeft;
    }

    public void setSugarLeft(int sugarLeft) {
        this.sugarLeft = sugarLeft;
    }

    public void setCoffeLeft(int coffeLeft) {
        this.coffeLeft = coffeLeft;
    }

    public void setWaterLeft(double waterLeft) {
        this.waterLeft = waterLeft;
    }

    public void setAllProductsTo0(){
        this.setSugarLeft(0);
        this.setWaterLeft(0.0);
        this.setCoffeLeft(0);
    }


}
