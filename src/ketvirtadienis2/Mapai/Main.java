package ketvirtadienis2.Mapai;

import sun.util.resources.cldr.ext.LocaleNames_mg;

import java.util.*;

/**
 * Created by rkore on 10/12/2017.
 */
public class Main {

    public static void printMapElements (Map <String, List>map, String key){
       printListsFromMap(map.get(key));
    }

    public static void printListsFromMap( List <String> zodziai){
        for(String str: zodziai){
            System.out.println(str);
        }

    }

    private static List<String> zodziai = new ArrayList<>();
    private static List <String> teiginiais = new ArrayList<>();
    private static Map <String, List> zodziuMapas = new HashMap<>();

    private Persosn person;


    public static void main(String[] args) {


        zodziai.add("pirmas");
        zodziai.add("Anmtas");
        zodziai.add("asttuntas");

        teiginiais.add("Teigiu");
        teiginiais.add("Liepiu");
        teiginiais.add("kreipiuosi");

        zodziuMapas.put("Zosziai",zodziai);
        zodziuMapas.put("Teiginiai", teiginiais);

        zodziuMapas.get(teiginiais.get(0));

        printMapElements(zodziuMapas,"Teiginiai");


//        lambda
        zodziuMapas.forEach((raktas, reiksme) -> System.out.println(reiksme));

        List <Persosn> persons =
                Arrays.asList(

        new Persosn("Rytis","28756348","tolimmsmsm",false),
        new Persosn("vytiss","28723458","tm",false),
        new Persosn("tomas","28456348","smsm",false),
        new Persosn("julius","28723348","msm",false)
        );

    }
}
