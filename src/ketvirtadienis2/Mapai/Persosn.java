package ketvirtadienis2.Mapai;

/**
 * Created by rkore on 10/12/2017.
 */
public class Persosn {
    private String name;
    private String telefonoNr;
    private String pavarde;

    public Persosn(String name, String telefonoNr, String pavarde, boolean convixted) {
        this.name = name;
        this.telefonoNr = telefonoNr;
        this.pavarde = pavarde;
        this.convixted = convixted;
    }

    private boolean convixted;

    public String getName() {
        return name;
    }

    public String getTelefonoNr() {
        return telefonoNr;
    }

    public String getPavarde() {
        return pavarde;
    }

    public boolean isConvixted() {
        return convixted;
    }
}
