package treciadienis2.Interfeisai;

/**
 * Created by rkore on 10/11/2017.
 */
public interface Informavimas {
    void siustiPranesima(String pranesimas);
    void siusitVIPPranesima(String ypatingas);
    void siustiAsmeniniPranesima (String darbuotojoRaktazodis, String asmeninisPranesimas);

}
