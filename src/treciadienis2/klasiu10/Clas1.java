package treciadienis2.klasiu10;

/**
 * Created by rkore on 10/11/2017.
 */
public class Clas1 {
    private Clas2 antraObject;



    public Clas1() {
        this.antraObject = new Clas2();
    }

    public void liepkRektSeptintam(){
        antraObject.liepkRektSeptintam();
    }

    public void imkFanta (Fantas fantas){
        antraObject.imkFanta(fantas);
    }

}
