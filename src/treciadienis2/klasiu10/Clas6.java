package treciadienis2.klasiu10;

/**
 * Created by rkore on 10/11/2017.
 */
public class Clas6 {
    private Clas7 septintasO;

    public Clas6() {
        this.septintasO = new Clas7();
    }

    public void liepkRektSeptintam(){
        septintasO.rekti();
    }

    public void imkFanta (Fantas fantas) {
        septintasO.imkFanta(fantas);
    }


}
